import React from 'react';
import TicTacToePage from './pages/TicTacToePage';
import './styles/App.css';

const App = () => {
  return (
    <div className="App">
      <TicTacToePage />
    </div>
  );
}

export default App;
