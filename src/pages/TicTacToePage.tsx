import React, { useState } from 'react'
import TicTacToeGame from '../components/TicTacToeGame';

const TicTacToePage = () => {
    const [xIsTurn, setXIsTurn] = useState(true);

    return (
        <div>
            <TicTacToeGame 
                xIsTurn={xIsTurn}
                setXIsTurn={setXIsTurn}
            />
        </div>
    )
}

export default TicTacToePage;
