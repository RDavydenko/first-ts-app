import React, { useState } from 'react';
import { IHistoryItem, SquareValue } from './TicTacToeGame';

interface IGameHistoryProps {
    history: Array<IHistoryItem>,
    setHistory: (history: Array<IHistoryItem>) => void,
    setXIsTurn: (xIsTurn: boolean) => void,
    setSquares: (squares: Array<SquareValue>) => void,
    updateHistoryCallback: () => void
}

const GameHistory = ({ history, setHistory, setXIsTurn, setSquares, updateHistoryCallback }: IGameHistoryProps) => {
    const click = (i: number) => {
        const snapshot = history[i];
        setHistory(history.slice(0, i));
        setSquares(snapshot.squares);
        setXIsTurn(snapshot.xIsTurn);
        updateHistoryCallback();
    };

    return (
        <div>
            {history.map((x, index) =>
                <div key={index}>
                    <button onClick={() => click(index)}>Перейти к ходу {index}</button>
                </div>
            )}
        </div>
    )
}

export default GameHistory;
