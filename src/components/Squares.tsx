import React, { useState } from 'react'
import Square from './Square';
import '../styles/Squares.css';
import { SquareValue } from './TicTacToeGame';

interface ISquaresProps {
    squares: Array<SquareValue>,
    onClick: (i: number) => void
}

const Squares = ({ squares, onClick } : ISquaresProps) => {
    const getSquareValue = (i: number) => {
        return squares[i];
    }
    return (
        <div>
            <div className="squares">
                <div className="squares-row">
                    <Square value={getSquareValue(0)} onClick={() => onClick(0)}/>
                    <Square value={getSquareValue(1)} onClick={() => onClick(1)}/>
                    <Square value={getSquareValue(2)} onClick={() => onClick(2)}/>
                </div>
                <div className="squares-row">
                    <Square value={getSquareValue(3)} onClick={() => onClick(3)} />
                    <Square value={getSquareValue(4)} onClick={() => onClick(4)} />
                    <Square value={getSquareValue(5)} onClick={() => onClick(5)} />
                </div>
                <div className="squares-row">
                    <Square value={getSquareValue(6)} onClick={() => onClick(6)} />
                    <Square value={getSquareValue(7)} onClick={() => onClick(7)} />
                    <Square value={getSquareValue(8)} onClick={() => onClick(8)} />
                </div>
            </div>
        </div>
    )
}

export default Squares;
