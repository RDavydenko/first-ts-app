import React, { useState } from 'react'
import '../styles/Squares.css';
import { calculateWinner } from '../utils/calc';
import GameHistory from './GameHistory';
import Squares from './Squares';

interface ISquaresProps {
    xIsTurn: boolean,
    setXIsTurn: (xIsTurn: boolean) => void
}

export interface IHistoryItem {
    squares: Array<SquareValue>,
    xIsTurn: boolean
}

export enum SquareValue {
    "X" = "X",
    "O" = "O",
    null = "null"
};

const TicTacToeGame = ({ xIsTurn, setXIsTurn }: ISquaresProps) => {
    const [squares, setSquares] = useState(Array<SquareValue>(9).fill(SquareValue.null))
    const [winner, setWinner] = useState<SquareValue>(SquareValue.null);
    const [history, setHistory] = useState<IHistoryItem[]>([])

    const clickBySquare = (i: number) => {
        if (winner === SquareValue.null && squares[i] === SquareValue.null) {
            updateHistory();

            squares[i] = xIsTurn ? SquareValue.X : SquareValue.O;
            setXIsTurn(!xIsTurn);
            setWinner(calculateWinner(squares));
        }
    }

    const updateHistory = () => {
        setHistory([...history, { squares: [...squares], xIsTurn: xIsTurn }]);
    }

    return (
        <div style={{margin: "30px"}}>
            {winner !== SquareValue.null && <h2>Победил игрок {winner}</h2>}
            {winner === SquareValue.null && <h2>Ход игрока {xIsTurn ? "X" : "O"}</h2>}
            <Squares
                squares={squares}
                onClick={clickBySquare}
            />
            <GameHistory
                history={history}
                setHistory={setHistory}
                setXIsTurn={setXIsTurn}
                setSquares={setSquares}
                updateHistoryCallback={() => setWinner(SquareValue.null)}
            />
        </div>
    )
}

export default TicTacToeGame;
