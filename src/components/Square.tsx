import React from 'react'
import '../styles/Square.css';
import { SquareValue } from './TicTacToeGame';

interface ISquareProps {
    value: SquareValue,
    onClick: () => void
}

const Square = ({value, onClick} : ISquareProps) => {
    return (
        <div className="square" onClick={onClick}>
            {value !== SquareValue.null && value}
        </div>
    )
}

export default Square
